package com.example.scroll

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        signinbutton.setOnClickListener {
            signin()
        }
    }
    private fun signin(){
        val mail = emailedit.text.toString()
        val pass = passwordedit.text.toString()
        val intent = Intent (this, profileact::class.java )
        if(mail.isNotEmpty() && pass.isNotEmpty()){
            signinbutton.isClickable = false
            startActivity(intent)
        }
        else{
            Toast.makeText(this,"Please Enter all fields",Toast.LENGTH_SHORT).show()
        }
    }
}