package com.example.scroll

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profileact.*

class profileact : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profileact)
        init()
    }

    private fun init() {
        Glide.with(this)
            .load("https://images.gr-assets.com/authors/1489686223p5/14847840.jpg")
            .into(image1)

        Glide.with(this)
            .load("https://agenda.ge/files/news/026/first-president-gamsakhurdia-ind.jpg")
            .into(image2)

        Glide.with(this)
            .load("https://archive.gov.ge/storage/images/kostava.jpg")
            .into(image3)
    }
}